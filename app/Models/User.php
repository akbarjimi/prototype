<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $guarded = [];

    public function businesses()
    {
        return $this->belongsToMany(
            User::class,'business_user','user_id','business_id',
            'id','id',__FUNCTION__
        );
    }


    public function tasks()
    {
        return $this->hasMany(Task::class,'user_id','id');
    }

    public function projects()
    {
        return $this->belongsToMany(
            Project::class,'project_user','user_id','project_id',
            'id','id','projects');
    }
}

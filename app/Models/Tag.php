<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];

    public function business()
    {
        return $this->belongsTo(Business::class,'business_id','id',__FUNCTION__);
    }

    public function task()
    {
        return $this->belongsToMany(
            Task::class,'tag_task','tag_id','task_id',
            'id','id',__FUNCTION__
        );
    }
}

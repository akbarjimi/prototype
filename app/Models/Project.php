<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = [];

    public function parent()
    {
        return $this->hasOne(Project::class,'parent_id','id');
    }

    public function children()
    {
        return $this->hasMany(Project::class,'parent_id','id');
    }

    public function root()
    {
        return $this->hasOne(Project::class,'root_id','id');
    }

    public function members()
    {
        return $this->belongsToMany(
            User::class,'project_user','project_id','user_id',
            'id','id',__FUNCTION__
        );
    }

    public function tasks()
    {
        return $this->hasMany(Task::class,'project_id','id');
    }

    public function business()
    {
        return $this->belongsTo(Business::class,'business_id','id');
    }
}

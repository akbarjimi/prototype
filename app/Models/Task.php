<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = [];

    public function business()
    {
        return $this->belongsTo(Business::class,'business_id','id',__FUNCTION__);
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id','id',__FUNCTION__);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id',__FUNCTION__);
    }

    public function project()
    {
        return $this->belongsTo(Project::class,'project_id','id',__FUNCTION__);
    }

    public function workflow()
    {
        return $this->belongsTo(Workflow::class,'workflow_id','id',__FUNCTION__);
    }

    public function workstatus()
    {
        return $this->belongsTo(Workstatus::class,'work_status_id','id',__FUNCTION__);
    }

    public function tags()
    {
        return $this->belongsToMany(
            Tag::class,'tag_task','tag_id','task_id',
            'id','id',__FUNCTION__
        );
    }
}

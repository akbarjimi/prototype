<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workstatus extends Model
{
    protected $guarded = [];

    public function workflow()
    {
        return $this->belongsTo(Workflow::class,'workflow_id','id');
    }
}

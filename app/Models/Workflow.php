<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    protected $guarded = [];

    public function business()
    {
        return $this->belongsTo(Business::class,'business_id');
    }

    public function workstatuses()
    {
        return $this->hasMany(Workstatus::class,'workflow_id','id');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class,'workflow_id','id');
    }
}

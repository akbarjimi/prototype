<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $guarded = [];

    public function owners()
    {
        return $this->belongsToMany(
            User::class,'business_user','business_id','user_id',
            'id','id',__FUNCTION__
        )->wherePivot('is_admin','=', true);
    }

    public function members()
    {
        return $this->belongsToMany(
            User::class,'business_user','business_id','user_id',
            'id','id',__FUNCTION__
        )
            ->wherePivot('is_admin','=', false);
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class,'business_user','business_id','user_id',
            'id','id',__FUNCTION__
        );
    }

    public function tags()
    {
        return $this->hasMany(Tag::class,'business_id','id');
    }

    public function projects()
    {
        return $this->hasMany(Project::class,'business_id','id');
    }

    public function workflows()
    {
        return $this->hasMany(Workflow::class,'business_id','id');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class,'business_id','id');
    }
}

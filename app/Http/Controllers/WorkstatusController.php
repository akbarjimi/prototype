<?php

namespace App\Http\Controllers;

use App\Models\Workstatus;
use Illuminate\Http\Request;

class WorkstatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Workstatus  $workstatus
     * @return \Illuminate\Http\Response
     */
    public function show(Workstatus $workstatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Workstatus  $workstatus
     * @return \Illuminate\Http\Response
     */
    public function edit(Workstatus $workstatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Workstatus  $workstatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Workstatus $workstatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Workstatus  $workstatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workstatus $workstatus)
    {
        //
    }
}

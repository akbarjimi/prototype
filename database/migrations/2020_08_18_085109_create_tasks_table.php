<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('business_id');
            $table->unsignedBigInteger('creator_id');
            $table->unsignedBigInteger('workflow_id');
            $table->unsignedBigInteger('work_status_id');
            $table->unsignedBigInteger('project_id')->nullable();
            /**
             * @fire mission; danger close.
             */
            $table->unsignedBigInteger('user_id')->nullable();

            $table->unsignedBigInteger('parent_id')->nullable();

            $table->string('name');
            $table->time('time')->nullable();
            $table->unsignedBigInteger('cost')->default(0);
            $table->boolean('completed')->default(false);
            $table->timestamp('due_date')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}

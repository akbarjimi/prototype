<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('business_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->boolean('parent_id')->nullable();
            $table->unsignedTinyInteger('order')->default(0);
            $table->string('name');
            $table->string('slug')->unique();
            $table->boolean('private')->default(false);
            $table->unsignedBigInteger('budget')->default(0);

            $table->timestamp('start')->useCurrent();
            $table->timestamp('finish')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('archived_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

        Schema::create('project_user', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id');
            $table->unsignedBigInteger('user_id');

            $table->boolean('manager')->default(false);
            $table->boolean('view_works')->default(false);

            $table->boolean('create_doc')->default(false);
            $table->boolean('view_doc')->default(false);

            $table->boolean('view_finance')->default(false);
            $table->boolean('create_finance')->default(false);

            $table->boolean('create_task')->default(false);
            $table->boolean('assign_task')->default(false);

            $table->unique(['project_id', 'user_id'], 'project_user_unique');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
        Schema::dropIfExists('project_user');
    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Workflow;
use Faker\Generator as Faker;

$factory->define(Workflow::class, function (Faker $faker) {
    return [
        'business_id' => null,
        'name' => $faker->word,
        'desc' => $faker->sentences(1, true),
    ];
});

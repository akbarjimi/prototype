<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tag;
use Faker\Factory;

$faker = Factory::create('fa_IR');
$factory->define(Tag::class, function () use ($faker) {
    return [
        'label' => $faker->colorName,
        'business_id' => null,
    ];
});

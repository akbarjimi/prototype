<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'business_id' => null,
        'creator_id' => null,
        'project_id' => null,
        'user_id' => null,
        'workflow_id' => null,
        'name' => $faker->sentences(3, true),
        'time' => null,
        'cost' => 0,
        'completed' => false,
        'due_date' => now()->toDateTimeString(),
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'business_id' => null,
        'name' => $faker->words(3, true),
        'desc' => $faker->sentences(1, true),
    ];
});

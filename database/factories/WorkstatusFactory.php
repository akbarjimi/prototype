<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Workstatus;
use Faker\Generator as Faker;

$factory->define(Workstatus::class, function (Faker $faker) {
    return [
        'workflow_id' => null,
        'name' => $faker->word,
        'done' => false,
    ];
});

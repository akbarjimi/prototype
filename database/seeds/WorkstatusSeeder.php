<?php

use App\Models\Workflow;
use App\Models\Workstatus;
use Illuminate\Database\Seeder;

class WorkstatusSeeder extends Seeder
{
    public function run()
    {
        $statuses1 = [
                ['name' => 'todo','done' => false],
                ['name' => 'doing','done' => false],
                ['name' => 'test','done' => false],
                ['name' => 'done','done' => true],
            ];

        $statuses2 = [
                ['name' => 'open','done' => false],
                ['name' => 'close','done' => false],
                ['name' => 'resolved','done' => true],
                ['name' => 'reject','done' => false],
            ];


        $workflows = Workflow::select('id')->pluck('id')->toArray();
        foreach ($workflows as $workflow) {
            $even = $workflow % 2 === 0;
            $statuses = [
                'workflow_id' => $workflow,
                'name' => $faker->word,
                'done' => false,
            ];
            $workflow->workstatuses()->createMany($even ? $statuses1 : $statuses2);
        }
    }
}

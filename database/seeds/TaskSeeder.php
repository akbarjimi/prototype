<?php

use App\Models\Business;
use App\Models\Tag;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskSeeder extends Seeder
{
    public function run()
    {
        /** @var User $owner */
        // every project has it's own tasks

        /**
         * Every task has it's own tags, which must be present in business that this tags belongs to it.
         */
        $businesses = Business::with('owners', 'projects')->get();
        $tasks = [];

        $left = count($businesses);
        $now = now()->toDateTimeString();
        $future = now()->addDays(10)->toDateTimeString();
        foreach ($businesses as $business) {
            foreach ($business->owners as $owner) {
                foreach ($business->projects as $project) {
                    foreach ($business->members as $member) {
                        for ($i = 1; $i <= 30; $i++) {
                            foreach ($business->workflows as $workflow) {
                                $status = $workflow->workstatuses->random();
                                $tasks[] = [
                                    'business_id' => $business->id,
//                                    'creator_id' => $owner->id,
//                                    'project_id' => $project->id,
//                                    'user_id' => $member->id,
//                                    'workflow_id' => $workflow->id,
//                                    'work_status_id' => $status->id,
//                                    'name' => 'Task #' . rand(111111, 999999),
//                                    'time' => "10:00:00",
//                                    'cost' => 10000,
//                                    'completed' => $status->done,
//                                    'due_date' => $status->done ? $now : $future,
                                ];
                            }
                        }
                    }
                }
            }

            $this->command->info(--$left . " Business is left.");
        }

        dd('here');
        $chunks = array_chunk($tasks, 1000);
        $number = count($chunks);
        foreach ($chunks as $value) {
            DB::table('tasks')->insert($value);
            $this->command->info(--$number . " Chunk is left.");
        }
    }
}

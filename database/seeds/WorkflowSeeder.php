<?php

use App\Models\Business;
use App\Models\Workflow;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkflowSeeder extends Seeder
{
    public function run()
    {
        // every business has it's own workflows
        $businesses = Business::select('id')->pluck('id');

        $relations = [];
        foreach ($businesses as $business) {
            $relations[] = factory(Workflow::class)->raw([
                'business_id' => $business,
                'name' => 'bug',
            ]);

            $relations[] = factory(Workflow::class)->raw([
                'business_id' => $business,
                'name' => 'feature',
            ]);
        }

        DB::table('workflows')->insert($relations);
    }
}

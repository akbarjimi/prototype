<?php

use App\Models\Business;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class BusinessSeeder extends Seeder
{
    public function run()
    {
        /** @var Collection $owners */

        $total_owners = 250;
        $owners = User::inRandomOrder()->take($total_owners)->get();

        // Create business to the number of managers
        Business::insert(factory(Business::class, $total_owners)->raw());
        $business_ids = Business::select('id')->pluck('id');

        /**
         * Every business has one or more owners, which might be the owner of other business as well
         */
        $this->insertAdmins($business_ids, $owners);


        /**
         * Every business has one or more members, which might be the member of other business as well
         */
        $this->insertMembers($business_ids);
    }

    /**
     * @param $business_ids
     * @param Collection $owners
     * @return array
     */
    public function insertAdmins($business_ids, Collection $owners)
    {
        $relations = [];
        foreach ($business_ids as $id) {
            $relations[] = [
                'business_id' => $id,
                'user_id' => $owners->random()->id,
                'is_admin' => true,
            ];
        }

        // Specify the percentage of managers who have two or more businesses
        $multiple = ceil(count($relations) / 100 * 30);
        foreach (Arr::random($relations, $multiple) as $relation) {
            $relations[] = [
                'business_id' => $relation['business_id'],
                'user_id' => $owners->except($relation['user_id'])->random()->id,
                'is_admin' => true,
            ];
        }

        DB::table('business_user')->insert($relations);
    }

    /**
     * @param $business_ids
     */
    public function insertMembers($business_ids): void
    {
        /** @var Collection $member_ids */
        $member_ids = User::whereDoesntHave('businesses')->select('id')->pluck('id');
        $relations = [];
        $current = [];
        foreach ($member_ids as $index => $id) {
            $relations[] = [
                'business_id' => $business = $business_ids->random(),
                'user_id' => $id,
            ];

            $current[$business][] = $index;
        }

        $multiple = ceil(count($relations) / 100 * 20);
        foreach (Arr::random($relations, $multiple) as $relation) {
            $relations[] = [
                'business_id' => $relation['business_id'],
                'user_id' => $member_ids->except($current[$relation['business_id']])->random(),
            ];
        }

        DB::table('business_user')->insert($relations);
    }
}

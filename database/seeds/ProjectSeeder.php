<?php

use App\Models\Business;
use App\Models\Project;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ProjectSeeder extends Seeder
{
    public function run()
    {
        /**
         * Every business has one or more projects
         * Every projects might have sub projects
         */
        $businesses = Business::select('id')->pluck('id');

        /**
         * One level projects
         */
        $this->insertMainProjects($businesses);

        /**
         * Multi level projects
         * Be careful with private projects
         */
        $this->insertChildProjects();

        $this->insertGrandChildrenProjects();

        /**
         * Assign people to the projects
         * * Be careful with private projects
         */
        $this->assignPeopleToProjects();
    }

    /**
     * @param $businesses
     * @return void
     */
    public function insertMainProjects($businesses): void
    {
        $projects = [];
        foreach ($businesses as $business) {
            $projects[] = factory(Project::class, 5)->raw([
                'business_id' => $business,
                'private' => rand(1, 100) <= 10,
            ]);
        }
        DB::table('projects')->insertOrIgnore(Arr::collapse($projects));
    }

    /**
     * @return void
     */
    public function insertChildProjects(): void
    {
        $projects = Project::inRandomOrder()->select('id', 'business_id')->take(100)->get()->toArray();
        $child_projects = [];
        foreach ($projects as $project) {
            $child_projects[] = factory(Project::class)->raw([
                'parent_id' => $project['id'],
                'root_id' => $project['id'],
                'business_id' => $project['business_id'],
                'private' => rand(1, 100) <= 10,
            ]);
        }
        DB::table('projects')->insert($child_projects);
    }

    public function insertGrandChildrenProjects(): void
    {
        $projects = Project::inRandomOrder()
            ->select('id', 'business_id', 'parent_id')
            ->whereNotNull('parent_id')
            ->take(25)->get()->toArray();
        $grand_child_projects = [];
        foreach ($projects as $project) {
            $grand_child_projects[] = factory(Project::class)->raw([
                'parent_id' => $project['id'],
                'root_id' => $project['parent_id'],
                'business_id' => $project['business_id'],
                'private' => rand(1, 100) <= 10,
            ]);
        }
        DB::table('projects')->insertOrIgnore($grand_child_projects);
    }

    public function assignPeopleToProjects(): void
    {
        //         Schema::create('')
        //            $table->unsignedBigInteger('project_id');
        //            $table->unsignedBigInteger('');

        $project_members = [];
        $projects = Project::with('parent','children','root','business')->get();
        foreach ($projects as $project) {
            // detect the main projects
            if ($project->children->isNotEmpty()) {
                continue;
            }

            $business_owner = $project->business->owners->random();
            $business_members = $project->business->members->random($project->business->members->count() - 1);
            $project_members[] = [
                'project_id' => $project->id,
                'user_id' => $business_owner->id,
            ];

            foreach ($business_members as $business_member) {
                $project_members[] = [
                    'project_id' => $project->id,
                    'user_id' => $business_member->id,
                ];
            }

            // todo : how to detect a project has sub project
            // child
            // grand child
        }
        DB::table('project_user')->insert($project_members);
    }
}

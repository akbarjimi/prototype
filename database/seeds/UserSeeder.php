<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::insert(
            factory(User::class, 5000)->raw()
        );
    }
}

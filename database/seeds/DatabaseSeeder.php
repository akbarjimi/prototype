<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
         $this->call([
             UserSeeder::class,
             BusinessSeeder::class,
             TagSeeder::class,
             ProjectSeeder::class,
             WorkflowSeeder::class,
             WorkstatusSeeder::class,
//             TaskSeeder::class,
         ]);
    }
}

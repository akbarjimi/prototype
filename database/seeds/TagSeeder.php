<?php

use App\Models\Business;
use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagSeeder extends Seeder
{
    public function run()
    {

        /**
         * Every business has it's own tags, which might be present in another business with the same name
         */
        $tags = factory(Tag::class, 10)->make()->pluck('label')->toArray();
        $businesses = Business::select('id')->pluck('id');

        $relations = [];
        foreach ($businesses as $business) {
            shuffle($tags);
            foreach ($tags as $tag) {
                $relations[] = [
                    'label' => $tag,
                    'business_id' => $business,
                ];
            }
        }
        DB::table('tags')->insertOrIgnore($relations);
    }
}
